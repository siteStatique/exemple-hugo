---
title: "Exemple de site avec Hugo et GitLab Pages"

description: "Un site Hugo construit avec GitLab Pages"
cascade:
  featured_image: '/images/gohugo-default-sample-hero-image.jpg'
---

Ce site web est alimenté par [GitLab Pages](https://about.gitlab.com/features/pages/)
et [Hugo](https://gohugo.io), et peut être construit en moins d'une minute.

Il utilise le thème [`ananke`](https://github.com/theNewDynamic/gohugo-theme-ananke) qui prend en charge le contenu de la page d'accueil.

Editez `/content/en/_index.md` pour changer ce qui apparaît ici. Supprimez `/content/en/_index.md` si vous ne voulez pas de contenu ici.

Rendez-vous sur le [projet GitLab] (https://gitlab.com/pages/hugo) pour commencer.